

import torch
import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt


P_AR = 3
T = 10
D = 1
#H = 400 # horizon, number of steps to forecast

P_TRAIN = 0.7

LR = 0.1
N_EPOCHS = 1000


# Data
n_series = 1000
series = np.sin(0.1*np.arange(n_series)) + np.random.randn(n_series)*0.1

# Plot
plt.plot(series)
plt.show()

# Create tensor X and y
N = len(series) - T

X = []
y = []
for ii in range(N):
    X.append(series[ii:ii+T])
    y.append(series[ii+T])


# Train - Test
m = int(P_TRAIN*N)
X_train = X[:m]
#X_test = X[m:]  # do not use
y_train = y[:m]
y_test = y[m:]
series_train = series[:m+T]

inputs = torch.from_numpy(np.array(X_train).astype(np.float32))
targets = torch.from_numpy(np.array(y_train).reshape(-1,1).astype(np.float32))



# Model
M1 = 32
model = nn.Sequential(
    nn.Linear(T, M1), 
    nn.ReLU(),
    nn.Linear(M1, M1), 
    nn.ReLU(),
    nn.Linear(M1, 1),
)
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters())  # SGD, Adagrad, RMSprop, Adam (betas=(0., 0.999))



# Train the model
losses = []
for it in range(N_EPOCHS):
    optimizer.zero_grad()
    
    # Forward pass
    outputs = model(inputs)
    loss = criterion(outputs, targets)
    losses.append(loss.item())
    
    # Backward and optimize
    loss.backward()
    optimizer.step()
    
print(loss.item())
# predicted = model(inputs).detach().numpy()
# print(model.bias.data.numpy(), model.weight.data.numpy())
plt.plot(losses)
plt.yscale('log')
plt.show()


# Predict
series_train_test = series_train.copy().tolist()
#y_test2 = []
y_predict = []
N2 = len(series_train) - T
for ii in range(N2, N-T-1):
    inputs_test = series_train_test[ii:ii+T]
    inputs_test = torch.from_numpy(np.array(inputs_test).astype(np.float32))

    outputs = model(inputs_test).item()
    series_train_test.append(outputs)
    y_predict.append(outputs)
#    y_test2.append(series[ii+T])
    
    # X.append(series[ii:ii+10])
    
plt.plot(y_test, label='target')
plt.plot(y_predict, label='prediction')
#plt.legend()
plt.show()







    
