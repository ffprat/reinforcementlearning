"""
MCTS Code

MCDT: Monte Carlo Decision Tree
Policy: NN_theta(s) -> p(a), for al a in g.valid_actions()
Value: NN_theta(s) -> v, scalar value
Edge: (s,a)

The NN is the same: NN_theta(s) -> (p(a), v)
MCTS(s) -> (pi(a), z), where z is the final reward and the tree is explored using the PUCT or UCB1 method

We want to minimize the difference between our prediction and the results we obtain from MCTS
    loss function (J) = (z-v)^2 - pi.T * np.log(p) + c*|theta|^2 = squared error + cross-entropy loss + regularization

MCDT:
    Each tree in the node has the following edges (s,a) for all the following actions
        N(s,a), Q(s,a), P(s,a)
        Number of visits, mean value (expected reward), policy or p(a)
        
    
Links:
    http://joshvarty.github.io/AlphaZero/
    
    
LEARNINGS
    As we can't do the reverse action, we have to start the tree always at s0
    
TODO
    Define J and plot it for each iteration, not only the reward
    Decide if the edge_space has to be reset at each interatin or not, currently it is mantained
    Where to start the tree after each iteration, currently at s2
"""

import random
import numpy as np
from copy import deepcopy

import matplotlib.pyplot as plt

ROWS = 3
COLS = 4
ACTIONS = ['U','D','R','L']
GAMMA = 1  # Discount factor
PENALTY = -0.5
S0 = (2,1)

RESET_TREE = True
ALPHA = 0.05  # Model Learning Rate

C_UCB1 = 64  # Regulated the degree of exploration, default=2
EPS_DECAY = 0.995




# =============================================================================
# Funcs
# =============================================================================
map_actions = {}
for ii, action in enumerate(ACTIONS):
    map_actions[action] = ii
    
def to_string(V, type_element='float') -> str:
    rows_str: list[str] = []
    for row in V:
        if type_element=='float':
            row = ["{: .2f}".format(col) for col in row]  # str(round(col,2))
        elif type_element=='int':
            row = ["{:>7}".format(col) for col in row]
        elif type_element=='object':
            pass
        rows_str.append("  ".join(row))
    return "\n".join(rows_str)




# =============================================================================
# Classes
# =============================================================================
class Gridworld():
    
    def __init__(self, rows: int, cols: int):
        """Defines 3-dim array:
            Dim2: rows
            Dim3: cols
        """

        # Terminal States
        terminal = [[0] * cols for _  in range(rows)]
        terminal[0][cols-1] = 1
        terminal[1][cols-1] = 1
        self.terminal = terminal
        
        # Impossible States
        impossible = [[0] * cols for _  in range(rows)]
        impossible[1][1] = 1
        self.impossible = impossible
        
        # Rewards
        rewards = [[PENALTY] * cols for _  in range(rows)]
        rewards[0][cols-1] = 1
        rewards[1][cols-1] = -1
        self.rewards = rewards
        
        # Valid Actions
        self.actions_space = [
            [['R','D'],['R','L'],['R','D','L'],[]],
            [['U','D'],[],['U','D','R'],[]],
            [['R','U'],['R','L'],['R','U','L'],['U','L']],
        ]
        
        # All states
        all_states = []
        all_states_not_terminal = []
        for i in range(rows):
            for j in range(cols):
                if self.is_impossible((i,j)): continue
                all_states.append((i,j))
                if self.is_terminal((i,j)): continue
                all_states_not_terminal.append((i,j))
        self.all_states =  all_states
        self.all_states_not_terminal = all_states_not_terminal
        
        
    def is_terminal(self, s):
        return self.terminal[s[0]][s[1]]
    
    def is_impossible(self, s):
        return self.impossible[s[0]][s[1]]
    
    def valid_actions(self, s):
        return self.actions_space[s[0]][s[1]]
    
    def reward(self, s):
        return self.rewards[s[0]][s[1]]
    
    def new_status(self, s, a, b_windy=True):
        if a not in self.valid_actions(s):
            print('Not a valid action')
            raise Exception('Not a valid action')
        x = s[0]
        y = s[1]
        if a=='U':
            x += -1
        elif a=='D':
            x += 1
        elif a=='R':
            y += 1
        elif a=='L':
            y += -1
        prob_s2 = [[0] * COLS for _  in range(ROWS)]
        prob_s2[x][y] = 1
        
        # Windy
        if s[0]==1 and s[1]==2 and a=='U' and b_windy:
            prob_s2 = [[0] * COLS for _  in range(ROWS)]
            prob_s2[x][y] = 0.5
            prob_s2[1][3] = 0.5
        
        # New status
        p_new_s = [prob_s2[i][j] for i, j in self.all_states if prob_s2[i][j]]
        p_new_s /= np.sum(p_new_s)
        new_s = [(i, j) for i, j in self.all_states if prob_s2[i][j]]
        ii_a = np.argmax(np.random.rand() <= np.cumsum(p_new_s))
        s = new_s[ii_a]
        return prob_s2, s
    
    

class MCTS():
    
    def expand(self, s):
        for a in g.valid_actions(s):
            self.edge_space[(s,a)] = {
                'z': 0,
                'n': 0,
                'p': model.predict_p(s)[map_actions[a]],
            }
            
    def is_leaf(self, s):
        a = g.valid_actions(s)[0]
        return (s, a) not in self.edge_space.keys()
            
    def select_edge(self, s):
        u_scores = []
        valid_a = g.valid_actions(s)
        if len(valid_a)==1: return valid_a[0]
        N = sum([self.edge_space[(s,a)]['n'] for a in valid_a])
        if N==0: return valid_a[0]
        for a in valid_a:
            z = self.edge_space[(s,a)]['z']
            n = self.edge_space[(s,a)]['n']
            p = self.edge_space[(s,a)]['p']
            if n==0: return a
            # ucb1 = np.sqrt(2 * np.log(N) / n)
            c_puct = 1.25 + np.log((N+19652+1)/19652)
            puct = c_puct * p * np.sqrt(N) / (1+n)
            u_scores.append(z + puct)
        return valid_a[np.argmax(u_scores)]

    def rollout(self, s, max_steps=20, method='greedy'):
        z = 0
        path_rollout = []
        
        # Simulation until terminal state or max_steps
        for _ in range(max_steps):
            if g.is_terminal(s): 
                path_rollout.append((s,'X'))
                break
            
            # Rollout policy
            if method=='random':
                valid_a = g.valid_actions(s)
                a = random.choice(valid_a)   
            elif method=='greedy':
                a = policy.eps_greedy(g, model, s)
            
            _, s2 = g.new_status(s, a)
            z += g.reward(s2)
            path_rollout.append((s,a))
            s = s2
            
        return z, path_rollout
    
    def backpropagate(self, tree_path, z):
        """Update self.edge_space for all the parents"""
        n_steps = 0
        for s, a in tree_path:
            mc.edge_space[(s,a)]['n'] += 1
            n = mc.edge_space[(s,a)]['n']
            alpha = min(0.5, max(0.01, 1/n))  # combination between mean and exponential moving average
            z_old = mc.edge_space[(s,a)]['z']
            # print(s, a, n_steps, (z-n_steps*PENALTY))
            mc.edge_space[(s,a)]['z'] += alpha * ((z-n_steps*PENALTY) - z_old)
            n_steps += 1

    
    def tree_traversal(self, s, max_steps=20):
        tree_path = []
        z = 0
        for _ in range(max_steps):            
            # s2 is leaf
            if self.is_leaf(s):
                self.expand(s)
                continue
            
            # Select new action
            a = self.select_edge(s)
            tree_path.append((s,a))
            
            # New state
            _, s2 = g.new_status(s, a)
            z += g.reward(s2)
            
            # s2 is terminal
            if g.is_terminal(s2):
                break

            # s2 is already expanded but with 0 visits -> rollout
            n = self.edge_space[(s,a)]['n']
            if n==0: 
                z_rollout, _ = self.rollout(s2, method='greedy')
                z += z_rollout
                break
            
            # continue with the tree traversal
            s = s2
        
        return tree_path, z
    
    def pi_a(self, s):
        pi_a = [self.edge_space[(s,a)]['n'] if (s,a) in self.edge_space.keys() else 0 for a in ACTIONS]
        pi_a = (np.array(pi_a) / sum(pi_a)).round(3)
        return pi_a.tolist()
    
    def vectors_MCTS(self):
        all_states = []
        Pi_a = []
        X = []
        Z = []
        for s, a in self.edge_space.keys():
            if s in all_states: continue
            all_states.append(s)
            X.append(model.state2x(s).tolist()[0])
            Pi_a.append(self.pi_a(s))
        for s in all_states:
            zs = [self.edge_space[(s,a)]['z']*self.edge_space[(s,a)]['n'] for a in g.valid_actions(s)]
            ns = [self.edge_space[(s,a)]['n'] for a in g.valid_actions(s)]
            Z.append(sum(zs)/sum(ns))
        return all_states, np.array(X), np.array(Pi_a), np.array(Z).reshape(-1,1)
    
    def runner(self, s0, n_sims=5000, n_trees=100):
        s = s0
        J_evol = []
        self.edge_space = {}
        
        for ii in range(n_sims):
            if RESET_TREE:
                self.edge_space = {}
            
            # run MCTS
            for _ in range(n_trees):
                tree_path, z = mc.tree_traversal(s)
                # print(z, tree_path)
                mc.backpropagate(tree_path, z)
            
            # Update the model
            s_vector, X, Pi_a, Z = self.vectors_MCTS()
            model.update_w(s_vector, X, Pi_a, Z)
            if s==s0:
                J_evol.append((Z[0,0]-model.predict_v(s))**2)
                
            policy.update_eps()
            
            # Action(s) given the current model or given the current MCTS
            # a, _ = model.policy(s, a_old)
            # a_old = a
            
            # Start from s2
            # _, s2 = g.new_status(s, a)
            # s = s2 if not g.is_terminal(s2) else s0
            
        return J_evol
            


class MyModel():
    def __init__(self):
        # self.w = {a: np.zeros(ROWS*COLS).reshape(-1,1) for a in ACTIONS}
        self.w = np.zeros((ROWS*COLS, len(ACTIONS)))
        self.w_z = np.zeros((ROWS*COLS, 1))
            
    def state2x(self, s):
        V = [[0] * COLS for _  in range(ROWS)]
        i, j = s
        V[i][j] = 1
        x = np.array(V)
        x = x.reshape(1, COLS*ROWS)
        return x
    
    # def vectors_model(self, s_vector):
    #     P_a = []
    #     for s in s_vector:
    #         P_a.append(self.policy(s)[1].tolist())
    #     V = []
    #     return np.array(P_a), V
    
    def update_w(self, s_vector, X, Pi_a, Z):
        alpha = ALPHA
        
        # Update w
        for a in ACTIONS:
            w_a = self.w[:,map_actions[a]].reshape(-1,1)
            # yhat = model.vectors_model(s_vector)[0][:,map_actions[a]].reshape(-1,1)  # Normalized
            yhat = np.dot(X, w_a)
            y = Pi_a[:,map_actions[a]].reshape(-1,1)
            
            w_a = w_a + alpha * np.dot(X.T, (y - yhat))
            self.w[:,map_actions[a]] = w_a.T
        
        # Update w_z
        Zhat = np.dot(X, self.w_z)
        self.w_z += alpha * np.dot(X.T, (Z - Zhat))
        
    def predict_v(self, s):
        return np.dot(self.state2x(s), self.w_z)[0][0]
    
    def predict_p(self, s):
        p = np.dot(model.state2x(S0), self.w)[0]
        if sum(p) == 0:
            p = [1/len(ACTIONS) for _ in ACTIONS]
            
        p_valid = [p[ii] if ACTIONS[ii] in g.valid_actions(s) else 0 for ii in range(len(ACTIONS))]
        p_valid = np.array(p_valid)
        p_valid = p_valid / np.sum(p_valid)
        return p_valid
            
          
            
# =============================================================================
# Policies
# =============================================================================

class Policy():
    
    def __init__(self):
        self.eps_min = 0.01
        self.eps_decay = EPS_DECAY
        self.eps = 1
        self.N_s = [[0] * COLS for _  in range(ROWS)]
        self.N_s_a = [[[0] * len(ACTIONS) for _  in range(COLS)] for _ in range(ROWS)]
        
    def greedy(self, g, model, s):
        if g.is_terminal(s):
            return ACTIONS[0]
        return ACTIONS[np.argmax(model.predict_p(s))]
    
    def eps_greedy(self, g, model, s):
        if np.random.random() > self.eps:
            a = ACTIONS[np.argmax(model.predict_p(s))]
        else:
            a = random.choice(g.valid_actions(s))
        return a
    
    def update_eps(self):
        self.eps = max(self.eps_min, self.eps_decay * self.eps)
        
    def ucb1(self, g, model, s):
        Qs = model.predict_p(s)
        i, j = s
        Us = [
            np.sqrt( C_UCB1 * np.log(
                1+self.N_s[i][j]) / (1+self.N_s_a[i][j][map_actions[ai]])
            )
            for ai in ACTIONS
        ]
        return ACTIONS[np.argmax(np.array(Qs) + np.array(Us))]
    
    def update_N(self, s, a):
        i, j = s
        self.N_s[i][j] += 1
        self.N_s_a[i][j][map_actions[a]] += 1
        


    
def policy_old(self, s):
    """ Policy from model
        Only greedy actions
        TODO, random when probabilities are equal
    """
    if g.is_terminal(s):
        return ACTIONS[0]
    
    valid_a = g.valid_actions(s)
    i, j = s
    a = None
    Qs = []
    for ai in ACTIONS:
        w_a = self.w[:,map_actions[ai]].reshape(-1,1)
        V_s_a = np.dot(self.state2x(s), w_a)[0][0]
        Qs.append(V_s_a)
    
    # action from greedy policy
    a = ACTIONS[np.argmax(Qs)]
    if a not in valid_a or a is None or sum(Qs)==0:
        a = random.choice(valid_a)
    
    # Obtain p_a
    p_a = np.array(Qs)
    if sum(Qs) == 0:
        p_a = [1/len(valid_a) if a in valid_a else 0 for a in ACTIONS]
    else:
        p_a = p_a / sum(p_a)
        
    return a, p_a


# =============================================================================
# MAIN
# =============================================================================
if __name__ == '__main__':
    
    g = Gridworld(ROWS, COLS)
    mc = MCTS()
    model = MyModel()
    policy = Policy()
    
    z_evol = mc.runner(S0, 100, 100)
    
    # Values from model
    v = model.predict_v(S0)
    p = np.dot(model.state2x(S0), model.w)
    
    # Values from last MCTS
    s_vector, X, Pi_a, Z = mc.vectors_MCTS()
    z = Z[0][0]
    pi = Pi_a[0]
    
    # Los Function
    # J = (z-v)**2 - np.dot(pi,np.log(p))

    # Portada
    print('Start position:', S0)
    print('One play path:', mc.rollout(S0, method='greedy')[1])
    print('Expected Value from MCTS: {:.2f}'.format(z))
    print('Expected Value from Model: {:.2f}'.format(v))
    plt.plot(z_evol)
    



        
        
            
        
                
            