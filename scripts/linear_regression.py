"""

Linear Regression
  y_hat = X w
  J = 1/(2m) *  (y_hat - y)^2 + lambda/(2m) (L*w)^2
  gradJ = 1/m + X.t * (y_hat - y) + lambda/m*L*w
  
  Analytical Solution = np.linalg.solve(np.dot(X.T, X), np.dot(X.T, Y))
  
Gradient Descent methods
  Stochastic Gradient Descent: w <- w - LR * gradJ
  Momentum
  AdaGrad (Addaptative)


See: y. Neural network parameters are optimized by stochastic gradient 
descent with momentum and learning rate annealing


Gradient descent theory: 
    https://towardsdatascience.com/neural-network-optimizers-from-scratch-in-python-af76ee087aab
    AdaGrad: https://dmitrijskass.netlify.app/2021/04/03/gradient-descent-with-linear-regression-from-scratch/
    https://stackabuse.com/gradient-descent-in-python-implementation-and-theory/
    Momentum: https://bitmask93.github.io/ml-blog/Linear-Regression&Gradient-Descent/
"""


import numpy as np
import matplotlib.pyplot as plt
from sklearn.kernel_approximation import RBFSampler

a = -5
b = 4.3
N = 100

# Gradient Descent
LAMBDA = 0      # Regularization Term
LR = 1  # Learning Rate
EPS = 1e-6
METHOD = 'adam'  # sdg, momentum, adagrad, rmsprop, adam

np.random.seed(123)
N_ITERS = 300




def append_ones(x: np.array):
    N = x.shape[0]
    ones = np.ones(N).reshape(-1,1)
    return np.hstack((ones, x))

# =============================================================================
# Model Class
# =============================================================================
class LinearRegression():
    
    def __init__(self):
        self.Jevol = []
        self.N = None
        self.D = None
        self.w = None
        self.v = None
        self.g = None
        self.t = 1
        
    def init_params(self, X, Y):
        self.N = X.shape[0]
        self.D = X.shape[1]
        self.w = np.zeros((self.D,1))
        # self.w[0,0] = np.mean(Y)
        self.v = np.zeros((self.D,1))
        self.g = np.zeros((self.D,1))
        
        
    def feature_expansion(self, X, feature_method='none'):
        
        if feature_method=='none':
            return X
        
        # Polinomial
        elif feature_method=='polinomial':
            print('  Feature expansion polinomial not implemented')
            return X
        
        # KBF kernel (Radial Basis Function)
        elif feature_method=='rbf':
            rbf_feature = RBFSampler(gamma=1, random_state=1)
            X_features = rbf_feature.fit_transform(X)
            # X_features2 = rbf_feature.transform(X)
            return X_features
        
        
    def predict(self, X, w):
        return np.dot(X, w)
    
    def J(self, X, Y, w):
        m = len(Y)
        L = np.diag(np.ones(X.shape[1]))
        error = Y - self.predict(X, w)
        r_term = np.dot(L, w)
        return 1/(2*m) * np.dot(error.T, error)[0][0] + LAMBDA/(2*m) * np.dot(r_term.T, r_term)[0][0]
    
    def gradientJ(self, X, Y, w):
        m = len(Y)
        L = np.diag(np.ones(X.shape[1]))
        L[0,0] = 0
        gradJ = 1/m * np.dot(X.T, self.predict(X, w) - Y) + LAMBDA/N * np.dot(L, w)
        # gradJ_norm = gradJ /  np.linalg.norm(gradJ)
        return gradJ
    
    def normal_equation(self, X, Y):
        # The Normal Equation
        # theta = np.dot(np.linalg.inv(np.dot(X.T, X)), np.dot(X.T, Y))
        theta = np.linalg.solve(np.dot(X.T, X), np.dot(X.T, Y))
        return theta
    
    def gradient_descent(self, w, method='sdg'):
        gradJ = self.gradientJ(X, Y, w)
    
        # Stochastic Gradient Descent (SGD)
        if method=='sdg':
            w -= LR * gradJ
            
        # Gradient Descent with Momentum
        elif method=='momentum':
            momentum = 0.3
            self.v = -LR * gradJ + momentum * self.v  # SGD + momentum * previous step
            w += self.v 
        
        # AdaGrad
        elif method=='adagrad':
            self.g += gradJ**2  # increases at each itereation
            w -= LR / (EPS + np.sqrt(self.g)) * gradJ
            
        # RMSprop, like AdaGrad but g is updated using the exponential moving average
        elif method=='rmsprop':
            beta2 = 0.99
            self.v = beta2 * self.v + (1-beta2) * gradJ**2
            w -= LR / (EPS + np.sqrt(self.g)) * gradJ
            
        # Adam
        elif method=='adam':
            beta1 = 0.3  # def = 0.9
            beta2 = 0.999
            
            self.g = beta1 * self.g + (1-beta1) * gradJ  # en la literatura se llama m
            self.v = beta2 * self.v + (1-beta2) * gradJ**2
            
            m_cor = self.g / (1-beta1**self.t)
            v_cor = self.v / (1-beta2**self.t)
            
            w -= LR * m_cor / (EPS + np.sqrt(v_cor))
            
        
        return w
    
    
    def fit(self, X, y, max_iters=N_ITERS):
        self.init_params(X, Y)
        
        for _ in range(max_iters):
            self.Jevol.append(self.J(X,Y,self.w))
            self.w = model.gradient_descent(self.w, method=METHOD)
            self.t += 1
            
        return self.w
    
    def portada(self, X, Y, w):
        
        # R2
        error = Y - self.predict(X, w)
        SSres =  np.dot(error.T, error)[0][0]
        
        error_mu = Y - np.mean(Y)
        SStot =  np.dot(error_mu.T, error_mu)[0][0]
        
        R2 = 1 - SSres/SStot
        
        print(f'R2: {R2:.3f}')
        
        w_exact = self.normal_equation(X, Y)
        print('Normal Equation:\ta={:.3f}; b={:.3f}'.format(w_exact[0][0], w_exact[1][0]))  
        print('Grdadient Descent:\ta={:.3f}; b={:.3f}'.format(w[0][0], w[1][0]))  

        print('Cost function, optimal: {:.2f}'.format(self.J(X, Y, w_exact)))
        print('Cost function, model: {:.2f}'.format(self.J(X, Y, w)))

        # plot LR
        x_plot = np.array([1, np.min(x), 1, np.max(x)]).reshape((2,2))
        plt.plot(X[:,1], Y, '.')
        plt.plot(x_plot[:,1], self.predict(x_plot, w) )
        plt.show()

        # Plot Jevol
        plt.plot(self.Jevol)
        # plt.xscale('log')
        plt.yscale('log')
        plt.plot()
            



# =============================================================================
# MAIN
# =============================================================================
if __name__ == '__main__':
    
    # Data
    x = np.linspace(1, 50, N).reshape((N,1))
    X = append_ones(x)
    Y = (a + b*x) + 4* np.random.normal(0, 3, (N,1))

    # Model
    model = LinearRegression()
    # X = model.feature_expansion(X, feature_method='none')
    w = model.fit(X, Y)
    model.portada(X, Y, w)





    
    
    
    

    
    