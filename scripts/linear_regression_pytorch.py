

"""
Predict Uncertainty
    We create a NN with two outputs: (mu, log(sigma**2)) or (mean, log-variance)

def criterion(outputs, targets):
    mu = outputs[0]
    v = torch.exp(outputs[1])  # where v = sigma**2
    
    
    c = 1/2 * torch.log(2*np.pi*v)
    f = 1/(2*v) * (targets - mu)**2
    
    retrun torch.mean(c + f)

"""
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt


LR = 1  # Learning Rate
n_epochs = 100

# Data
a = -5
b = 4.3
N = 100
np.random.seed(123)

X = np.linspace(1, 50, N).reshape(-1,1)
Y = (a + b*X) + 4* np.random.normal(0, 3, (N,1))


# Funcs
"""
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)
"""
class Normalize():
    def __init__(self):
        self.mean_x = None
        self.sd_x = None
        
    def fit(self, X):
        self.mean_x = X.mean(axis=0).reshape(1,-1)
        self.sd_x = X.std(axis=0).reshape(1,-1)
        
    def transform(self, X):
        mean_x_m = np.full(X.shape, self.mean_x)
        sd_x_m = np.full(X.shape, self.sd_x)
        X_norm = (X-mean_x_m) / sd_x_m
        return X_norm
    
    def fit_transform(self, X):
        self.fit(X)
        X_norm = self.transform(X)
        return X_norm


# Data to Torch 
norm = Normalize()
norm.fit(X)
Xnorm = norm.transform(X)
inputs = torch.from_numpy(X.astype(np.float32))
targets = torch.from_numpy(Y.astype(np.float32))


# Model
model = nn.Linear(X.shape[1], Y.shape[1])  # Input dim, output dim
criterion = nn.MSELoss()  # cross-entropy loss in classification problems (or binary CE loss =BCELoss)
optimizer = torch.optim.Adam(model.parameters(), lr=LR, betas=(0.3, 0.999))  # SGD, Adagrad, RMSprop, Adam


# Train the model
losses = []
for it in range(n_epochs):
    optimizer.zero_grad()
    
    # Forward pass
    outputs = model(inputs)
    loss = criterion(outputs, targets)
    losses.append(loss.item())
    
    # Backward and optimize
    loss.backward()
    optimizer.step()
    
    
    
print(loss.item())
# predicted = model(inputs).detach().numpy()
print(model.bias.data.numpy(), model.weight.data.numpy())
plt.plot(losses)
plt.yscale('log')
plt.plot()
    
    



