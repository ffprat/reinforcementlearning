"""
Gridworld AlphaZero, Policy Gradient method with MCTS

Outputs and targets
    Outputs, from DQN: (p(a), v)
    Targets, from MCTS: (pi(a), z)

Custom loss functions
    def forward:
        ...
        return y1, y2
    
    def mse(outputs, targets):
        p, v = outputs
        pi, Z = targets
        J = (z-v)**2 - np.dot(pi.T, log(p))
        
"""


import random
import numpy as np
from copy import deepcopy

import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
from torchsummary import summary
# import torchvision.transforms.functional as TF
# TF.to_tensor(train_dataset.data)

ROWS = 3
COLS = 4
ACTIONS = ['U','D','R','L']
GAMMA = 1  # Discount factor
ALPHA = 0.01  # Factor for the exponential MA
EPS = 0.1  # Epsilon-Greedy method
SMALL_ENOUGH = 1e-3 # threshold for convergence
PENALTY = -0.3
S0 = (2,1)
N_SIM = 1000
C_UCB1 = 64  # Regulated the degree of exploration, default=2
EPS_DECAY = 0.995

# Replay Buffer
MAX_RB_SIZE = 16
RB_BATCH_SIZE = 8
N_UPDATE_MODEL = 10

# CNN
IN_CHANNELS = 1
CHANNELS = 1  # out channels of the first conv layer
K = 1
STRIDE = 1  # for small images use stride=1 (default value)
PADDING = 0   # 0 for valid, and 1 for same. Use 1 with small images
LR = 0.01
# M1 = 512




# =============================================================================
# Funcs
# =============================================================================
map_actions = {}
for ii, action in enumerate(ACTIONS):
    map_actions[action] = ii
    
def state2x(s):
    V = [[0] * COLS for _  in range(ROWS)]
    i, j = s
    V[i][j] = 1
    x = np.array(V)
    x = x.reshape(1, COLS*ROWS)
    return x

def state2torch(s):
    """ Transforms the state (as tuple) into a 3dim tensor
        Shape: N x Channels x H x W
    """
    X = torch.zeros((1,1,ROWS,COLS))
    X[0,0,s[0],s[1]] = 1
    return X

def to_string(V, type_element='float') -> str:
    rows_str: list[str] = []
    for row in V:
        if type_element=='float':
            row = ["{: .2f}".format(col) for col in row]  # str(round(col,2))
        elif type_element=='int':
            row = ["{:>7}".format(col) for col in row]
        elif type_element=='object':
            pass
        rows_str.append("  ".join(row))
    return "\n".join(rows_str)




# =============================================================================
# Classes
# =============================================================================
class Gridworld():
    
    def __init__(self, rows: int, cols: int):
        """Defines 3-dim array:
            Dim2: rows
            Dim3: cols
        """

        # Terminal States
        terminal = [[0] * cols for _  in range(rows)]
        terminal[0][cols-1] = 1
        terminal[1][cols-1] = 1
        self.terminal = terminal
        
        # Impossible States
        impossible = [[0] * cols for _  in range(rows)]
        impossible[1][1] = 1
        self.impossible = impossible
        
        # Rewards
        rewards = [[PENALTY] * cols for _  in range(rows)]
        rewards[0][cols-1] = 1
        rewards[1][cols-1] = -1
        self.rewards = rewards
        
        # Valid Actions
        self.actions_space = [
            [['R','D'],['R','L'],['R','D','L'],[]],
            [['U','D'],[],['U','D','R'],[]],
            [['R','U'],['R','L'],['R','U','L'],['U','L']],
        ]
        
        # All states
        all_states = []
        all_states_not_terminal = []
        for i in range(rows):
            for j in range(cols):
                if self.is_impossible((i,j)): continue
                all_states.append((i,j))
                if self.is_terminal((i,j)): continue
                all_states_not_terminal.append((i,j))
        self.all_states =  all_states
        self.all_states_not_terminal = all_states_not_terminal
        
        
    def is_terminal(self, s):
        return self.terminal[s[0]][s[1]]
    
    def is_impossible(self, s):
        return self.impossible[s[0]][s[1]]
    
    def valid_actions(self, s):
        return self.actions_space[s[0]][s[1]]
    
    def reward(self, s):
        return self.rewards[s[0]][s[1]]
    
    def step(self, s, a, b_windy=True):
        if a not in self.valid_actions(s):
            print('Not a valid action')
            raise Exception('Not a valid action')
        x = s[0]
        y = s[1]
        if a=='U':
            x += -1
        elif a=='D':
            x += 1
        elif a=='R':
            y += 1
        elif a=='L':
            y += -1
        prob_s2 = [[0] * COLS for _  in range(ROWS)]
        prob_s2[x][y] = 1
        
        # Windy
        if s[0]==1 and s[1]==2 and a=='U' and b_windy:
            prob_s2 = [[0] * COLS for _  in range(ROWS)]
            prob_s2[x][y] = 0.5
            prob_s2[1][3] = 0.5
        
        # New status
        p_new_s = [prob_s2[i][j] for i, j in self.all_states if prob_s2[i][j]]
        p_new_s /= np.sum(p_new_s)
        new_s = [(i, j) for i, j in self.all_states if prob_s2[i][j]]
        ii_a = np.argmax(np.random.rand() <= np.cumsum(p_new_s))
        s = new_s[ii_a]
        r = self.reward(s)
        return s, r
    
    def reset(self):
        S0 = [(2,1), (0,0), (2,0), (2,3)]
        return random.choice(S0)

# =============================================================================
# Model
# =============================================================================
# Conv arithmetic with 3 layers
def conv_arithmetic(h, k , stride, padding):
    dilatation = 1
    return int(np.floor((h + 2*padding - dilatation*(k-1) - 1) / stride + 1))

H1 = conv_arithmetic(ROWS, K, STRIDE, PADDING)
W1 = conv_arithmetic(COLS, K, STRIDE, PADDING)
# H2 = conv_arithmetic(H1, K, STRIDE, PADDING)

# Parameters dense NN
# D = CHANNELS * H1 * W1
C = len(ACTIONS)
D = ROWS * COLS


# Explore the model
if False:
    s = (2,1)
    X = state2torch(s)
    model_test = nn.Sequential(
        nn.Conv2d(IN_CHANNELS, CHANNELS, kernel_size=K, stride=STRIDE),
        nn.ReLU(),
        # nn.MaxPool2d(kernel_size=(H1,W1)),  # stride=None (same as kernel), padding=0
    )
    aa = model_test(X)
    aa2 = torch.flatten(X, 1, -1)
    print(X.shape)
    print(aa.shape)
    print(aa2.shape)
    
class CNN(nn.Module):
    """ Creates a custom model for de model: linear -> ReLU -> linear """
    def __init__(self, IN_CHANNELS, CHANNELS, K, STRIDE, D, C):
        super().__init__()  # super(CNN, self)
        self.conv = nn.Sequential(
            nn.Conv2d(IN_CHANNELS, CHANNELS, kernel_size=K, stride=STRIDE),
            nn.ReLU(),
            # nn.BatchNorm2d(CHANNELS),
            # nn.MaxPool2d(kernel_size=(H1,W1)),  # Global Max Pool
        )
        self.dense = nn.Sequential(
            # nn.Dropout(0.2),
            nn.Linear(D, C),  # or nn.Linear(D, M1) 
            # nn.Softmax(),  # ReLU, Softmax
        )
    
    def forward(self, x):
        """ Define the whole architecture, functions like F.relu() or F.dropout() can be used here too"""
        # x = self.conv(x)
        x = torch.flatten(x, 1, -1)  # keep the first dimension (number of observations) and flatten the rest ([1,-1])
        #x = x.view(x.size(0), -1)  # the same, but it requires the lenght of the first dim
        #x = torch.max(torch.max(x, 2)[0], 2)[0]  # I want to keep the first two dimension, N and CHANNELS, the rest H and W are reduced by taking the maximum
        x = self.dense(x)
        return x


def Q_NN(g, model, s):
    X = state2torch(s)
    # yhat =torch.max(model(X),1)[1]
    # yhat = F.softmax(model(X), dim=1).detach().numpy()[0,:]  # for probabilities
    yhat = model(X).detach().numpy()[0,:]  # For V
    
    for a in ACTIONS:
        if a not in g.valid_actions(s):
            yhat[map_actions[a]] = -np.inf
            
    return yhat.tolist()
    

# =============================================================================
# Policies
# =============================================================================

class Policy():
    
    def __init__(self):
        self.eps_min = 0.01
        self.eps_decay = EPS_DECAY
        self.eps = 1
        self.N_s = [[0] * COLS for _  in range(ROWS)]
        self.N_s_a = [[[0] * len(ACTIONS) for _  in range(COLS)] for _ in range(ROWS)]
        
    def greedy(self, g, model, s):
        if g.is_terminal(s):
            return ACTIONS[0]
        return ACTIONS[np.argmax(Q_NN(g, model, s))]
    
    def eps_greedy(self, g, model, s):
        if np.random.random() > self.eps:
            a = ACTIONS[np.argmax(Q_NN(g, model, s))]
        else:
            a = random.choice(g.valid_actions(s))
        return a
    
    def update_eps(self):
        self.eps = max(self.eps_min, self.eps_decay * self.eps)
        
    def ucb1(self, g, model, s):
        Qs = Q_NN(g, model, s)
        i, j = s
        Us = [
            np.sqrt( C_UCB1 * np.log(
                1+self.N_s[i][j]) / (1+self.N_s_a[i][j][map_actions[ai]])
            )
            for ai in ACTIONS
        ]
        return ACTIONS[np.argmax(np.array(Qs) + np.array(Us))]
    
    def update_N(self, s, a):
        i, j = s
        self.N_s[i][j] += 1
        self.N_s_a[i][j][map_actions[a]] += 1
        




# =============================================================================
# Best Policy - Control Problem
# =============================================================================

def update_replay_buffer(replay_buffer, s, a, r, s2, is_terminal):
    replay_buffer.append([s, a, r, s2, is_terminal])
    if len(replay_buffer) > MAX_RB_SIZE: 
        replay_buffer.pop(0)
    return replay_buffer

def batch_replay_buffer(g, model, model_freezed, replay_buffer):
    n_batch = min(len(replay_buffer), RB_BATCH_SIZE)
    batch = random.sample(replay_buffer, k=n_batch)
    inputs = torch.zeros((n_batch,1,ROWS,COLS))
    targets_a = torch.zeros((n_batch))
    ii_a = []
    ii = 0
    for s, a, r, s2, is_terminal in batch:
        inputs[ii,:,:,:] = state2torch(s)[0,:,:,:]
        targets_a[ii] = r + GAMMA * max(Q_NN(g, model_freezed, s2)) if not is_terminal else r
        ii_a.append(map_actions[a])
        ii += 1
    
    outputs = model(inputs)
    targets = outputs.detach().clone()
    targets[np.arange(targets.size(0)), ii_a] = targets_a
    # for a2_best, target in targets_a:
        
    return inputs, outputs, targets

def batch_GD(model, criterion, optimizer, model_freezed, replay_buffer):
    optimizer.zero_grad()
    
    inputs, outputs, targets = batch_replay_buffer(g, model, model_freezed, replay_buffer)
    
    loss = criterion(outputs, targets)
    loss.backward()
    optimizer.step()
    
    return loss        
        
def q_learning(g, model, policy, max_steps=20, max_iters=100):
    """ We move using a greedy strategy (a_best),
        but we learn Q with the best (greedy) policy (a2_best)
        Behaviour policy != Target policy
    """
    # deltas = []
    reward_per_episode = []
    losses = []
    replay_buffer = []
    model_freezed = deepcopy(model)
    
    for k in range(max_iters):
        s = g.reset()
        # biggest_change = 0
        n_steps = 0
        episode_reward = 0
        
        while not g.is_terminal(s) and n_steps<max_steps:
            n_steps += 1
            
            # Freeze model
            if k // N_UPDATE_MODEL:
                model_freezed = deepcopy(model)
            
            # Current state + action
            # a = policy.ucb1(g, model, s)
            a = policy.eps_greedy(g, model, s)
            policy.update_N(s, a)
            
            # Step: new state + best action
            s2, r = g.step(s, a)
            # a2_best = policy_w(g, s2, model, N_s_a, method='greedy')
            
            # Update Replay Buffer
            replay_buffer = update_replay_buffer(replay_buffer, s, a, r, s2, g.is_terminal(s2))
            
            # Gradient Descent with Experience Replay
            loss = batch_GD(model, criterion, optimizer, model_freezed, replay_buffer)
            
            # Update Values
            episode_reward += r
            losses.append(loss.item())
            s = s2
                        
        # deltas.append(biggest_change)
        policy.update_eps()
        reward_per_episode.append(episode_reward)
        
    # Policy and V
    V = [[0] * COLS for _  in range(ROWS)]
    policy_tab = [['X'] * COLS for _  in range(ROWS)]
    for i, j in g.all_states_not_terminal:
        V[i][j] = max(Q_NN(g, model, (i,j)))
        policy_tab[i][j] = policy.greedy(g, model, (i,j))
    
    return policy_tab, V, losses, reward_per_episode


    
        
    

# =============================================================================
# MAIN
# =============================================================================
if __name__ == '__main__':
    
    g = Gridworld(ROWS, COLS)
    
    # NN
    model = CNN(IN_CHANNELS, CHANNELS, K, STRIDE, D, C)
    policy = Policy()
    # print(summary(model, (IN_CHANNELS, H, W)))
    criterion = nn.MSELoss(reduction='sum')  # CrossEntropyLoss, MSELoss. MSELoss as I'm trying to predict V, Regression problem
    optimizer = torch.optim.Adam(model.parameters())

    # Prediction
    # V, N_s, w = policy_evaluation(g, w0, policy, start=S0, max_steps=20, max_iters=N_SIM)
    
    # Find Best policy - Control
    policy_tab, V, losses, reward_per_episode = q_learning(g, model, policy, max_steps=20, max_iters=N_SIM)
    
    # Portada    
    print(f'\nGamma: {GAMMA}')
    print('\nRewards:')
    print(to_string(g.rewards, type_element='float'))
    
    print('\nValue, expected sum of future rewards')
    print(to_string(V))
    # print('\nValue, from policy evaluation')
    # print(to_string(V_star))
    
    print('\nFinal policy:')
    print(to_string(policy_tab, type_element='object'))
    

    print('\nNumber of visits to each state')
    print(to_string(policy.N_s, type_element='int'))
        
    # plt.plot(reward_per_episode)
    # plt.title('Reward per episode')
    # plt.show()

        
