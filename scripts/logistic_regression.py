# -*- coding: utf-8 -*-
"""
Concepts Binary-class Classification
    model = linear + sigmoid, loss function = BCELoss (binary-cross entrolpy)
    model = linear, loss function = BCEWithLogitsLoss (combines a Sigmoid layer and the BCELoss), te predicted value is z!
    
Concepts Multi-class Classification
    one-hot encoding: create a vector for each class
    softmax: from z to probability, the sigmoid function is not used
    cross-entropy loss function, != from the binary cross-entropy loss function

Links
    Logistic Regression: https://towardsdatascience.com/softmax-regression-in-python-multi-class-classification-3cb560d90cb2
    NN: https://stackabuse.com/creating-a-neural-network-from-scratch-in-python-multi-class-classification/
"""
import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn import metrics

from linear_regression import append_ones



n_epochs = 500
LR = 0.01


dataset = datasets.load_iris()
X, y = dataset.data, dataset.target
# Standarize
X = append_ones(X)


C = len(dataset.target_names)
N, D = X.shape
w = np.zeros((D,C))


def one_hot(y, c):
    y_hot = np.zeros((len(y), c))
    y_hot[np.arange(len(y)), y] = 1
    return y_hot

def sigmoid(x):
    return 1/(1+np.exp(-x))

def softmax(z):
    expZ = np.exp(z - np.max(z))
    return expZ / expZ.sum(axis=1, keepdims=True)

def predict(X, w, one_hot_resu=True):
    z = np.dot(X, w)
    y_hat = softmax(z)
    if not one_hot_resu:
        y_hat = np.argmax(y_hat, axis=1)
    return y_hat

def J(X, y, w):
    """ Cross-entropy loss function
        J = - 1/N * sum_c (y * log(y_hat))
        As y is 0 or 1 is computationaly faster to calculate the log only for the real class
    """
    y_hat = predict(X, w)
    return - (1/N) * np.sum(np.log(y_hat[np.arange(len(y)), y]))
    
def gradientJ(X, y, w):
    y_hat = predict(X, w)
    return (1/N) * np.dot(X.T, (y_hat - one_hot(y, C)))
    


# Main
losses = []

for _ in range(n_epochs):
    gradJ = gradientJ(X, y, w)
    w = w - LR * gradJ 
    losses.append(J(X, y, w))
    
plt.plot(losses)
y_predict = predict(X, w, one_hot_resu=False)
conf_m = metrics.confusion_matrix(y, y_predict)
accuracy = np.sum(y==y_predict)/len(y)

print(conf_m)
print(f'Accuracy: {accuracy:.3f}')

    
    

    

    
    
    
    