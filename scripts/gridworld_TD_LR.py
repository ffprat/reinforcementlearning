# -*- coding: utf-8 -*-
"""
Temporal Difference Linear Regression, we move from a gridworld where all states are discrete and known
to a more general gridworld to inf states.

The value function (V) insted to be a table ort a dict now is a function from the state
V(s) = f_theta (s)
Where f is a supervised machine learning funciont and theta its parameteres. 
Normally we would use a Neural Network, but as this example is decoupled we will use a linear regression
with "one-hot encoding" as feature transformations. See RBF kernel as feature expansion

Prediction:
  V(s) = X w
  y = G = r + gamma * V(s2) = r + gamma * X2 w 
  w = w + alpha * X_t * ( y - X_t w )
  
Control:
  Q(s,:) = X w_a
  In this example we store w[a] as dict, so w is intependent from each action
  We could also use a 12x4 matrix
  When we update w we only update the values for the selecte action, 
  we can also set the new values for the other acrions equal to the old Q, so that values won't be updated

Gradient descent with Momentum
  v = mu * v - alpha * gradient
  w = w + v
  Where mu is the momentum term ~ 0.9 - 0.99
    

"""
import random
import numpy as np
from copy import deepcopy

import matplotlib.pyplot as plt

ROWS = 3
COLS = 4
ACTIONS = ['U','D','R','L']
GAMMA = 0.9  # Discount factor
ALPHA = 0.01  # Factor for the exponential MA
EPS = 0.1  # Epsilon-Greedy method
SMALL_ENOUGH = 1e-3 # threshold for convergence
PENALTY = -0.4
S0 = (2,1)
N_SIM = 10000



# =============================================================================
# Funcs
# =============================================================================
map_actions = {}
for ii, action in enumerate(ACTIONS):
    map_actions[action] = ii
    
def state2x(s):
    V = [[0] * COLS for _  in range(ROWS)]
    i, j = s
    V[i][j] = 1
    x = np.array(V)
    x = x.reshape(1, COLS*ROWS)
    return x

def to_string(V, type_element='float') -> str:
    rows_str: list[str] = []
    for row in V:
        if type_element=='float':
            row = ["{: .2f}".format(col) for col in row]  # str(round(col,2))
        elif type_element=='int':
            row = ["{:>7}".format(col) for col in row]
        elif type_element=='object':
            pass
        rows_str.append("  ".join(row))
    return "\n".join(rows_str)




# =============================================================================
# Classes
# =============================================================================
class Gridworld():
    
    def __init__(self, rows: int, cols: int):
        """Defines 3-dim array:
            Dim2: rows
            Dim3: cols
        """

        # Terminal States
        terminal = [[0] * cols for _  in range(rows)]
        terminal[0][cols-1] = 1
        terminal[1][cols-1] = 1
        self.terminal = terminal
        
        # Impossible States
        impossible = [[0] * cols for _  in range(rows)]
        impossible[1][1] = 1
        self.impossible = impossible
        
        # Rewards
        rewards = [[PENALTY] * cols for _  in range(rows)]
        rewards[0][cols-1] = 1
        rewards[1][cols-1] = -1
        self.rewards = rewards
        
        # Valid Actions
        self.actions_space = [
            [['R','D'],['R','L'],['R','D','L'],[]],
            [['U','D'],[],['U','D','R'],[]],
            [['R','U'],['R','L'],['R','U','L'],['U','L']],
        ]
        
        # All states
        all_states = []
        all_states_not_terminal = []
        for i in range(rows):
            for j in range(cols):
                if self.is_impossible((i,j)): continue
                all_states.append((i,j))
                if self.is_terminal((i,j)): continue
                all_states_not_terminal.append((i,j))
        self.all_states =  all_states
        self.all_states_not_terminal = all_states_not_terminal
        
        
    def is_terminal(self, s):
        return self.terminal[s[0]][s[1]]
    
    def is_impossible(self, s):
        return self.impossible[s[0]][s[1]]
    
    def valid_actions(self, s):
        return self.actions_space[s[0]][s[1]]
    
    def reward(self, s):
        return self.rewards[s[0]][s[1]]
    
    def new_status(self, s, a, b_windy=True):
        if a not in self.valid_actions(s):
            print('Not a valid action')
            raise Exception('Not a valid action')
        x = s[0]
        y = s[1]
        if a=='U':
            x += -1
        elif a=='D':
            x += 1
        elif a=='R':
            y += 1
        elif a=='L':
            y += -1
        prob_s2 = [[0] * COLS for _  in range(ROWS)]
        prob_s2[x][y] = 1
        
        # Windy
        if s[0]==1 and s[1]==2 and a=='U' and b_windy:
            prob_s2 = [[0] * COLS for _  in range(ROWS)]
            prob_s2[x][y] = 0.5
            prob_s2[1][3] = 0.5
        
        # New status
        p_new_s = [prob_s2[i][j] for i, j in self.all_states if prob_s2[i][j]]
        p_new_s /= np.sum(p_new_s)
        new_s = [(i, j) for i, j in self.all_states if prob_s2[i][j]]
        ii_a = np.argmax(np.random.rand() <= np.cumsum(p_new_s))
        s = new_s[ii_a]
        return prob_s2, s
    
    


# =============================================================================
# Policies
# =============================================================================
def epsilon_greedy(g, s, policy, eps=EPS):
    if g.is_terminal(s):
        return ACTIONS[0]
    
    i, j = s
    a = None
    if np.random.random() > eps: 
        a = policy[i][j]
        if a not in g.valid_actions(s):
            a = None
    
    if a is None:
        a = random.choice(g.valid_actions(s))
        
    return a

def policy_Q(g, s, Q, N, method='ucb1', eps=EPS):
    if g.is_terminal(s):
        return ACTIONS[0]
    
    i, j = s
    a = None
    
    if method=='greedy':
        Qs = [Q[i][j][map_actions[a]] for a in g.valid_actions(s)]
        a = g.valid_actions(s)[np.argmax(Qs)]
    
    elif method=='epsilon_greedy':
        if np.random.random() > eps: 
            Qs = [Q[i][j][map_actions[a]] for a in g.valid_actions(s)]
            a = g.valid_actions(s)[np.argmax(Qs)]
                
    elif method=='ucb1': 
        Qs = []
        for a in g.valid_actions(s):
            ucb1_score = (
                Q[i][j][map_actions[a]] + 
                np.sqrt( 2 * np.log(1+sum(N[i][j])) / (1+N[i][j][map_actions[a]]) )
            )
            Qs.append(ucb1_score)
        a = g.valid_actions(s)[np.argmax(Qs)]
    
    
    if a not in g.valid_actions(s) or a is None:
        a = random.choice(g.valid_actions(s))
        
    return a

def policy_w(g, s, w, N, method='ucb1', eps=EPS):
    if g.is_terminal(s):
        return ACTIONS[0]
    
    i, j = s
    a = None
    Qs = []
    Qs_ucb1 = []
    for ai in g.valid_actions(s):
        V_s_a = np.dot(state2x(s), w[ai])[0][0]
        ucb1_exp = np.sqrt( 2 * np.log(1+sum(N[i][j])) / (1+N[i][j][map_actions[ai]]) )
        Qs.append(V_s_a)
        Qs_ucb1.append(V_s_a + ucb1_exp)
    
    if method=='greedy':
        a = g.valid_actions(s)[np.argmax(Qs)]
    
    elif method=='epsilon_greedy':
        if np.random.random() > eps: 
            a = g.valid_actions(s)[np.argmax(Qs)]
                
    elif method=='ucb1': 
        a = g.valid_actions(s)[np.argmax(Qs_ucb1)]
    
    
    if a not in g.valid_actions(s) or a is None:
        a = random.choice(g.valid_actions(s))
        
    return a





# =============================================================================
# Policy Evaluation - Prediction Problem
# =============================================================================
def policy_evaluation(g, w, policy, start, max_steps=20, max_iters=100):
    """Temporal Diff LR"""
    w = w.copy()
    policy = deepcopy(policy)
    N_s = [[0] * COLS for _  in range(ROWS)]
    # deltas = []
    for k in range(max_iters):  # Iterations
        # biggest_change = 0
        s = start
        n_steps = 0
        while not g.is_terminal(s) and n_steps<max_steps:
            n_steps += 1
            
            # Current state + action
            i, j = s
            X = state2x(s)
            N_s[i][j] += 1
            V_hat = np.dot(X, w)[0][0]
            a = epsilon_greedy(g, s, policy, eps=EPS)
            
            # New status
            _, s2 = g.new_status(s, a)
            i2, j2 = s2
            X2 = state2x(s2)
            r = g.reward(s2)
            V_td = r + GAMMA * np.dot(X2, w)[0][0] if not g.is_terminal(s2) else r
            
            alpha = ALPHA
            w += alpha * X.T * (V_td - V_hat)  # update w proportional to the difference of the values predicted and from temporal difference
            # biggest_change = max(biggest_change, np.abs(v_old - V[i][j]))
            s = s2
        
        # deltas.append(biggest_change)
        
    # Policy and V
    V = [[0] * COLS for _  in range(ROWS)]
    for i, j in g.all_states_not_terminal:
        V[i][j] = np.dot(state2x((i,j)), w)[0][0] 
        
    return V, N_s, w




# =============================================================================
# Best Policy - Control Problem
# =============================================================================
def q_learning_LR(g, w: np.array, start, max_steps=20, max_iters=100):
    """ We move using a greedy strategy (a_best),
        but we learn Q with the best (greedy) policy (a2_best)
        Behaviour policy != Target policy
    """
    w = w.copy()
    N_s = [[0] * COLS for _  in range(ROWS)]
    N_s_a = [[[0] * len(ACTIONS) for _  in range(COLS)] for _ in range(ROWS)]
    deltas = []
    reward_per_episode = []
    
    for k in range(max_iters):
        s = start
        # biggest_change = 0
        n_steps = 0
        episode_reward = 0
        
        while not g.is_terminal(s) and n_steps<max_steps:
            n_steps += 1
            
            # Current state + action
            i, j = s
            a = policy_w(g, s, w, N_s_a, method='ucb1')  # epsilon_greedy, ucb1
            ii_a = map_actions[a]
            X = state2x(s)
            Q_hat = np.dot(X, w[a])[0][0]
            N_s[i][j] += 1
            N_s_a[i][j][ii_a] += 1
            
            # New state
            _, s2 = g.new_status(s, a)
            i2, j2 = s2
            a2_best = policy_w(g, s2, w, N_s_a, method='greedy')
            r = g.reward(s2)
            episode_reward += r
            
            # Update
            X2 = state2x(s2)
            Q_td = r + GAMMA * np.dot(X2, w[a2_best])[0][0] if not g.is_terminal(s2) else r
            alpha = min(0.5, max(ALPHA, 1/N_s_a[i][j][ii_a]))
            w[a] += alpha * X.T * (Q_td - Q_hat)
            # biggest_change = max(biggest_change, np.abs(q_old - Q[i][j][ii_a]))
            
            # Next iter
            s = s2
            
        # deltas.append(biggest_change)
        reward_per_episode.append(episode_reward)
        
    # Policy and V
    V = [[0] * COLS for _  in range(ROWS)]
    policy = [['X'] * COLS for _  in range(ROWS)]
    for i, j in g.all_states_not_terminal:
        V[i][j] = np.max([np.dot(state2x((i,j)), w[a])[0][0] for a in g.valid_actions((i,j))])
        policy[i][j] = policy_w(g, (i,j), w, N_s_a, method='greedy')
    
    return policy, w, V, deltas, N_s, reward_per_episode


        
    

# =============================================================================
# MAIN
# =============================================================================
if __name__ == '__main__':
    
    g = Gridworld(ROWS, COLS)
    
    # Initial policy with random actions
    # policy = [['X'] * COLS for _  in range(ROWS)]
    # for i, j in g.all_states_not_terminal:
    #     policy[i][j] = random.choice(g.valid_actions((i,j)))
    # policy = [
    #     ['R','R','R','X'],
    #     ['U','X','R','X'],
    # ]

    # Init V and Q with 0
    x_size = ROWS*COLS
    w0 = {
        'U': np.zeros(x_size).reshape((x_size,1)),
        'D': np.zeros(x_size).reshape((x_size,1)),
        'R': np.zeros(x_size).reshape((x_size,1)),
        'L': np.zeros(x_size).reshape((x_size,1)),
    }
    
    # Prediction
    # V, N_s, w = policy_evaluation(g, w0, policy, start=S0, max_steps=20, max_iters=N_SIM)
    
    # Find Best policy - Control
    policy, w, V, deltas, N_s, reward_per_episode = q_learning_LR(g, w0, start=S0,  max_steps=20, max_iters=N_SIM)
    
    # Portada    
    print('\nRewards:')
    print(to_string(g.rewards, type_element='float'))
    
    print('\nValue, expected sum of future rewards')
    print(to_string(V))
    # print('\nValue, from policy evaluation')
    # print(to_string(V_star))
    
    print('\nFinal policy:')
    print(to_string(policy, type_element='object'))
    

    print('\nNumber of visits to each state')
    print(to_string(N_s, type_element='int'))
        
    # plt.plot(reward_per_episode)
    # plt.title('Reward per episode')
    # plt.show()

        
