"""
Monte Carlo

We play an episode and then start from the terminal state
  T: G = r(s)
  T-1: G = r(s) + gamma * G
  T-2: G = r(s) + gamma * G
  
Then we update Q as
  Q = Q + alpha * (G - Q_old)
  
"""
import random
import numpy as np
from copy import deepcopy

ROWS = 3
COLS = 4
ACTIONS = ['U','D','R','L']
GAMMA = 0.9  # Discount factor (=0.9)
ALPHA = 0.1  # Factor for the exponential MA
EPS = 0.2  # Epsilon-Greedy method
SMALL_ENOUGH = 1e-3 # threshold for convergence
PENALTY = -0.4



# =============================================================================
# Funcs
# =============================================================================
map_actions = {}
for ii, action in enumerate(ACTIONS):
    map_actions[action] = ii


def new_status(g, s, a, b_windy=True):
    if a not in g.valid_actions(s):
        print('Not a valid action')
        raise Exception('Not a valid action')
    x = s[0]
    y = s[1]
    if a=='U':
        x += -1
    elif a=='D':
        x += 1
    elif a=='R':
        y += 1
    elif a=='L':
        y += -1
    prob_s2 = [[0] * COLS for _  in range(ROWS)]
    prob_s2[x][y] = 1
    
    # Windy
    if s[0]==1 and s[1]==2 and a=='U' and b_windy:
        prob_s2 = [[0] * COLS for _  in range(ROWS)]
        prob_s2[x][y] = 0.5
        prob_s2[1][3] = 0.5
    
    # New status
    p_new_s = [prob_s2[i][j] for i, j in g.all_states if prob_s2[i][j]]
    p_new_s /= np.sum(p_new_s)
    new_s = [(i, j) for i, j in g.all_states if prob_s2[i][j]]
    ii_a = np.argmax(np.random.rand() <= np.cumsum(p_new_s))
    s = new_s[ii_a]
    return prob_s2, s


def to_string(V, type_element='float') -> str:
    rows_str: list[str] = []
    for row in V:
        if type_element=='float':
            row = ["{: .2f}".format(col) for col in row]  # str(round(col,2))
        elif type_element=='int':
            row = ["{:>7}".format(col) for col in row]
        elif type_element=='object':
            pass
        rows_str.append("  ".join(row))
    return "\n".join(rows_str)




# =============================================================================
# Classes
# =============================================================================
class Gridworld():
    
    def __init__(self, rows: int, cols: int):
        """Defines 3-dim array:
            Dim2: rows
            Dim3: cols
        """

        # Terminal States
        terminal = [[0] * cols for _  in range(rows)]
        terminal[0][cols-1] = 1
        terminal[1][cols-1] = 1
        self.terminal = terminal
        
        # Impossible States
        impossible = [[0] * cols for _  in range(rows)]
        impossible[1][1] = 1
        self.impossible = impossible
        
        # Rewards
        rewards = [[PENALTY] * cols for _  in range(rows)]
        rewards[0][cols-1] = 1
        rewards[1][cols-1] = -1
        self.rewards = rewards
        
        # Valid Actions
        self.actions_space = [
            [['R','D'],['R','L'],['R','D','L'],[]],
            [['U','D'],[],['U','D','R'],[]],
            [['R','U'],['R','L'],['R','U','L'],['U','L']],
        ]
        
        # All states
        all_states = []
        all_states_not_terminal = []
        for i in range(rows):
            for j in range(cols):
                if self.is_impossible((i,j)): continue
                all_states.append((i,j))
                if self.is_terminal((i,j)): continue
                all_states_not_terminal.append((i,j))
        self.all_states =  all_states
        self.all_states_not_terminal = all_states_not_terminal
        
        
    def is_terminal(self, s):
        return self.terminal[s[0]][s[1]]
    
    def is_impossible(self, s):
        return self.impossible[s[0]][s[1]]
    
    def valid_actions(self, s):
        return self.actions_space[s[0]][s[1]]
    
    def reward(self, s):
        return self.rewards[s[0]][s[1]]
    



# =============================================================================
# Policy Evaluation - Prediction Problem
# =============================================================================
def MC_policy_eval(g, V, policy, max_episodes=20):
    V = deepcopy(V)
    for k in range(max_episodes):
        r_episode, a_episode, s_episode = play_episode(g, policy)
        G = 0
        for t in range(len(s_episode)-1)[::-1]:
            s = s_episode[t]
            r = r_episode[t+1]
            G = r + GAMMA * G
            if s_episode[t] not in s_episode[:t]:  # first-visit MC
                V[s[0]][s[1]] += 1/(k+1)*(G-V[s[0]][s[1]]) 
    return V

# =============================================================================
# Best Policy - Control Problem
# =============================================================================
def MC_control(g, policy, Q, start, eps=0, max_episodes=100):
    policy = deepcopy(policy)
    Q = deepcopy(Q)
    N_s_a = [[[0] * len(ACTIONS) for _  in range(COLS)] for _ in range(ROWS)]
    deltas = []
    visits_s = [[0] * COLS for _  in range(ROWS)]
    for k in range(max_episodes):
        r_episode, a_episode, s_episode = play_episode(g, policy, start=start, eps=eps)
        G = 0
        biggest_change = 0
        for t in range(len(s_episode)-1)[::-1]:
            i, j = s_episode[t]
            s = (i,j)
            r = r_episode[t+1]
            a = a_episode[t]
            G = r + GAMMA * G
            if s_episode[t] not in s_episode[:t]:  # first-visit MC
                visits_s[i][j] += 1
                ii_a = map_actions[a]
                old_qa = Q[i][j][ii_a]
                N_s_a[i][j][ii_a] += 1
                alpha = max(ALPHA, 1/(N_s_a[i][j][ii_a]))
                Q[i][j][ii_a] += alpha*(G-old_qa) 
                biggest_change = max(biggest_change, np.abs(old_qa - Q[i][j][ii_a]))
    
                # Update policy
                Qs = [Q[i][j][map_actions[a_val]] for a_val in g.valid_actions(s)]
                a_star = g.valid_actions(s)[np.argmax(Qs)]
                policy[i][j] = a_star
        deltas.append(biggest_change)
        
    # V_Star
    V = [[0] * COLS for _  in range(ROWS)]
    for i, j in g.all_states_not_terminal:
        V[i][j] = np.max([Q[i][j][map_actions[a]] for a in g.valid_actions((i,j))])
    
    return policy, Q, V, deltas, visits_s



# =============================================================================
# Agent
# =============================================================================
def play_episode(g, policy, start=None, eps=0, max_steps=20):
    if start is None:
        s = random.choice(g.all_states_not_terminal)
    else:
        s = start
    r_episode = [0]
    a_episode = []
    s_episode = [s]
    for k in range(max_steps):
        if g.is_terminal(s): break
        if np.random.random() < EPS:
            a = random.choice(g.valid_actions(s))
        else:
            a = policy[s[0]][s[1]]
            if a not in g.valid_actions(s): print('Warning: not a valid action, check policy')
        _, s = new_status(g, s, a)
        r = g.reward(s)
        r_episode.append(r)
        a_episode.append(a)
        s_episode.append(s)
    a_episode.append('')
    return r_episode, a_episode, s_episode



        
    

# =============================================================================
# MAIN
# =============================================================================
if __name__ == '__main__':
    
    g = Gridworld(ROWS, COLS)
    
    # Initial policy with random actions
    policy = [['X'] * COLS for _  in range(ROWS)]
    for i, j in g.all_states_not_terminal:
        policy[i][j] = random.choice(g.valid_actions((i,j)))

    # Init V and Q with 0
    V = [[0] * COLS for _  in range(ROWS)]
    Q = [[[0] * len(ACTIONS) for _  in range(COLS)] for _ in range(ROWS)]
    
    # Find Best policy - Control
    policy, Q, V, deltas, visits_s = MC_control(g, policy, Q, start=(2,3), eps=EPS, max_episodes=1000)
    V_star = MC_policy_eval(g, V, policy, max_episodes=20)
    
    
    # Portada    
    print('\nRewards:')
    print(to_string(g.rewards, type_element='float'))
    
    # print('\nInitial policy:')
    # print(to_string_policy(policy))
    print('\nFinal policy:')
    print(to_string(policy, type_element='object'))
    
    print('\nValue (s), expected sum of future rewards')
    print(to_string(V))
    print('\nValue, from policy evaluation')
    print(to_string(V_star))
    
    print('\nNumber of visits to each state')
    print(to_string(visits_s, type_element='int'))
        
    # plt.plot(deltas)
    # plt.title('Biggest Change V')
    # plt.show()

        
