"""
Sequential Data

    Data Structure (NxTxD)
        N: # samples
        T: # length (time steps in the sequence)
        D: # features
    Example Stock data, we could use a T=10 window to predict the following price
        in this example N would be the total number of windows, and D=1 (only the price)
    It's required constant-length sequences (or constant-length per batch), append 0 if it's not the case
    Forecast
        AR (Autoregressive): xt = w0 + w1*x_(t-1) + w2*x_(t-2) + w3*x_(t-3)
            We take windows of length 3 and try to predict the next value
            Intead of using a LR we could usa an ANN
            To forecast into the horizon, we need to do a one by one step aproach
    Simple RNN Equation (or Elman Unit):
        h_t = activation (X_t w_xh + h_(t-1) w_hh + b), where h_t is the hidden state vector (or layer)
        A RNN will do the forecast based an all previous steps:
            y_t = f(x_(t-1), x_(t-2), ..., x_1), vanishing gradient problem! difficult to lear from inputs too far back
        Output calculation
            ```
            h = zeros((N,M1))
            for t in tange(T):
                h = sigma( np.dot(X(t),Wxh) + b_xh + np.dot(h,W_hh) + b_hh )
                yhat = np.dot(h, W_dense) + b_dense  # we only care about the last one
            ```
    Modern RNN units
        LSTM (Long Short Term Memory)
        GRU (Gated Recurrent Unit), simplified version of LSTM
            h_t = f(x_t + h_(t-1))
            z_t = sigma(X_t w_xz + h_(t-1) w_hz + b_z), update gate vector
            r_t = sigma(X_t w_xr + h_(t-1) w_hr + b_r), reset gate vector
            h_t = (
                (1-z_t) * h_(t-1) + 
                z_t * tanh(X_t w_xh + (r_t*h_(t-1)) w_hh + b_r)
            ), where * is the element-wise multiplication
            Interpretation, weigthed average between h_(t-1) and tanh(...)
"""

import torch
import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt


T = 80  # window size, sequence length (L in pycharm)
D = 1  # number of features for each step (Hin in pycharm)
#H = 400 # horizon, number of steps to forecast
M1 = 8  # Hidden units (Hout in pycharm)
C = 1  # output units, it could be more than one for multiple regression or for classification

P_TRAIN = 0.7
LR = 0.1


LR = 0.1
N_EPOCHS = 1000


# Data
n_series = 1000
np.random.seed(123)
series = np.sin(0.1*np.arange(n_series)) + np.random.randn(n_series)*0.3

# Plot
plt.plot(series)
plt.show()

# Create tensor X and y
X = []
y = []
for t in range(n_series-T):
    X.append(series[t:t+T])
    y.append(series[t+T])


# Train and Test, X and y are lists
N = int(P_TRAIN*n_series)  # Number of samples (batch size)
X_train = X[:N]
X_test = X[N:]  # do not use
y_train = y[:N]
y_test = y[N:]
series_predict = series[:N+T].tolist()
# print(series_predict[-1], y_train[-1])

# To Torch
# input size: N x T, D, this is set with the option batch_first=True
inputs = torch.from_numpy(np.array(X_train).reshape(N,T,D).astype(np.float32))
targets = torch.from_numpy(np.array(y_train).reshape(-1,1).astype(np.float32))


# Model
# Explore the model
if False:
    model_test = nn.RNN(D, M1, num_layers =1, nonlinearity='relu', batch_first=True)
    aa, h = model_test(inputs, None)
    aa2 = aa[:,-1,:]
    print(inputs.shape)
    print(aa.shape) 
    print(aa2.shape) 
    print(h.shape)
    # aa2, h2 = model_test(inputs, h)
    
class SimpleRNN(nn.Module):
    """ ht (hidden layer) = f(xt + h_(t-1)) """
    def __init__(self, D, M1, C):
        super().__init__()  # super(CNN, self)

        self.rnn = nn.RNN(input_size=D, hidden_size=M1, num_layers=1, nonlinearity='relu', batch_first=True)
        self.dense = nn.Linear(M1, C)
    
    def forward(self, x):
        """ Define the whole architecture, functions like F.relu() or F.dropout() can be used here too"""
        # Initial hidden state
        h0 = torch.zeros((1,x.size(0),M1))  # .to(device)
        x, _ = self.rnn(x, h0)
        
        # we only want h(t) at the final time step
        x = self.dense(x[:,-1,:])
        return x
    
    
class ModernRNN(nn.Module):
    """ LSTM or GRU """
    def __init__(self, D, M1, C):
        super().__init__()  # super(CNN, self)

        self.rnn = nn.GRU(input_size=D, hidden_size=M1, num_layers=1, batch_first=True)
        self.dense = nn.Linear(M1, C)
    
    def forward(self, x):
        """ Define the whole architecture, functions like F.relu() or F.dropout() can be used here too"""
        # Initial hidden state
        # h0 = torch.zeros((1,x.size(0),M1))  # .to(device)
        # c0 = torch.zeros((1,x.size(0),M1))  # .to(device)
        x, _ = self.rnn(x)
        
        # we only want h(t) at the final time step
        x = self.dense(x[:,-1,:])
        return x

model = SimpleRNN(D, M1, C)  # inputs, hidden, output
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters())  # SGD, Adagrad, RMSprop, Adam (betas=(0., 0.999))


# Train the model
losses = []
for it in range(N_EPOCHS):
    optimizer.zero_grad()
    
    # Forward pass
    outputs = model(inputs)
    loss = criterion(outputs, targets)
    losses.append(loss.item())
    
    # Backward and optimize
    loss.backward()
    optimizer.step()

W_xh, W_hh, b_xh, b_hh = model.rnn.parameters()
print(loss.item())
# predicted = model(inputs).detach().numpy()
# print(model.bias.data.numpy(), model.weight.data.numpy())
plt.plot(losses)
plt.yscale('log')
plt.show()


# Predict
# y_predict = []
for t in range(N, n_series-T):
    inputs_test = series_predict[t:t+T]
    inputs_test = torch.from_numpy(np.array(inputs_test).reshape(1,T,1).astype(np.float32))

    outputs = model(inputs_test).item()
    series_predict.append(outputs)
    # y_predict.append(outputs)
    
plt.plot(y_test, label='target')
plt.plot(series_predict[N+T:], label='prediction')
#plt.legend()
plt.show()









