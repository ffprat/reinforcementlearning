"""
Convolutinoal NN

Convolution = Image modifier, like a filer (or kernel)
  It works by multiplying elemetwise the kernel matrix (2x2, 3x3, ..., KxK) to each submatrix of the picture 
    and then adding the results, similar to the dot product. In general K in [3, 5, 7]
  In an array of length N, if the filter has lenght K, the output size would be N-K+1.
    To have the output the same size as the input we can use padding, or external zeros.
    This is called "same", and the original is called "valid"
  Traditional convolution is a bit different (the kernel is flipped). 
    NN Colvolution is also referred as cross-correlation, as is very similar to the dot product.
  Convolution == pattern finder, like a filter 
  Example a=[a1, a2, a3, a4], w=[w1, w2]
    a * w, where * represent the convolution operator and a is a vertical vector
    [[w1 w2 0 0]
     [0 w1 w2 0]
     [0 0 w1 w2]] * a
  Convolution -> particular case of matrix multiplication, although it is not an efficient implementation
    In a NN: dot(X.T, w) -> could be considered as a convolution if w has a specific structure
  Color image: H x W x 3 (height, width, 3)
    We can apply different filters and then stack all of them together -> output: H x W X NF, wher NF is the number of filters
    We can do all this process in vectorized form W.shape = 3 x K x K x NF, where K is the size of the kernel (or filter)
  Pooling: downsample the image. In general by 2 without overlaping and taking the max, pool_size_K=2, stride=2
    This is useful as the output of a conv layer tells whether the "filter" has been found ot not
  Architecture
    Conv + pool -> conv + pool -> conv + pool -> ANN
    The image shrinks but the size of the filter stays the same, the filters look for patterns bigger and bigger
  Other Architecture:
    Strided conv -> strided conv -> strided conv -> ANN, where strided conv is a conv but skipping pixels between each step
  Hyperparameters += filter size (K), number of feature maps, pool_size. General values, but read papers to see what others are doing
    K in [3, 5, 7]
    Feature maps (or channels), increase them at each layer: 32, 64, 128, 128
    pool_size = 2
  ANN, we have a H x W x NF tensor, we need to reduce it to a 1d array. Two options
    Flatten: len(NF) = H x W x NF. Ok, but problematic if the input images have different sizes
    Global Max Pooling: len(1d array)=NF, where each value contains the max of the HxW tensor
  Code, conv 2d, as the image has 2 dim + channels (1 for gray or 3 for color)
    nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3, stride=2)
    conv1d: time series (time)
    conv3d: video (H, W, time)
  Dropout: During training, randomly zeroes some of the elements of the input tensor with probability p using samples from a Bernoulli distribution.
    like regularization
  Data Augmetnation: deep learning improves with higher amounts of data, unlike other algoriths that hit a plateau
    We can just create new data from the existing one: rotating, shift the image.
    In pytorch this can be done with the transform function.
  Batch Normalization: normalize the data after each layer
  To compare different images try: np.allclose(im1, im2)
"""

import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn import metrics

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
from torchsummary import summary
# import torchvision.transforms.functional as TF
# TF.to_tensor(train_dataset.data)



# Hyperparameters
CHANNELS = 32  # out channels of the first conv layer
K = 3
STRIDE = 2  # for small images use stride=1 (default value)
PADDING = 0   # 0 for valid, and 1 for same. Use 1 with small images
LR = 0.01


# Load data, data is sotred in data and targets
train_dataset = torchvision.datasets.MNIST(
    root='../data',
    train=True,
    download=True
)
test_dataset = torchvision.datasets.MNIST(
    root='../data',
    train=False,
    download=True
)
print(train_dataset.data.max())
print(train_dataset.data.shape)
print(test_dataset.data.shape)
H = train_dataset.data.size(1)
W = train_dataset.data.size(2)
IN_CHANNELS = 1
C = len(train_dataset.classes)

# Plot image
ii = 2
im = train_dataset.data[ii].numpy()
#plt.imshow(im, cmap='gray')

# Transfors the data model, this function belong to the dataset!
# Use transforms.Compose([aaa, aaa]) to compose different transformations
train_dataset.transform = transforms.ToTensor()
test_dataset.transform = transforms.ToTensor()


# Conv arithmetic with 3 layers
def conv_arithmetic(h, k , stride, padding):
    dilatation = 1
    return int(np.floor((h + 2*padding - dilatation*(k-1) - 1) / stride + 1))
H1 = conv_arithmetic(H, K, STRIDE, PADDING)
H2 = conv_arithmetic(H1, K, STRIDE, PADDING)
H3 = conv_arithmetic(H2, K, STRIDE, PADDING)

# Parameters dense NN
D = H3 * H3 * 4*CHANNELS
M1 = 512


# Custom Model 
"""
    Architecture1 (stride=2, padding=0): conv + relu + batchnorm -> conv + relu + batchnorm -> conv + relu + batchnorm -> Flattem -> Dense
    Architecture2 (stride=1, padding=1): 
        conv(32) + relu + batchnorm -> conv(32) + relu + batchnorm -> MaxPool -> 
        conv(64) + relu + batchnorm -> conv(64) + relu + batchnorm -> MaxPool -> 
        Flattem -> Dense
"""
class CNN(nn.Module):
    """ Creates a custom model for de model: linear -> ReLU -> linear """
    def __init__(self, IN_CHANNELS, CHANNELS, K, STRIDE, D, M1, C):
        super().__init__()  # super(CNN, self)
        self.conv = nn.Sequential(
            nn.Conv2d(IN_CHANNELS, CHANNELS, kernel_size=K, stride=STRIDE),
            nn.ReLU(),
            nn.BatchNorm2d(CHANNELS),
            nn.Conv2d(CHANNELS, 2*CHANNELS, kernel_size=K, stride=STRIDE),
            nn.ReLU(),
            nn.BatchNorm2d(2*CHANNELS),
            nn.Conv2d(2*CHANNELS, 4*CHANNELS, kernel_size=K, stride=STRIDE),
            nn.ReLU(),
            nn.BatchNorm2d(4*CHANNELS),
        )
        self.dense = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(D, M1),  # or nn.Linear(D, M1) 
            nn.ReLU(),
            nn.Dropout(0.2),
            nn.Linear(M1, C),
        )
    
    def forward(self, x):
        """ Define the whole architecture, functions like F.relu() or F.dropout() can be used here too"""
        x = self.conv(x)
        x = torch.flatten(x, 1, -1)  # keep the first dimension (number of observations) and flatten the rest ([1,-1])
        #x = x.view(x.size(0), -1)  # the same, but it requires the lenght of the first dim
        #x = torch.max(torch.max(x, 2)[0], 2)[0]  # I want to keep the first two dimension, N and CHANNELS, the rest H and W are reduced by taking the maximum
        x = self.dense(x)
        return x


#model = nn.Sequential(
#    nn.Conv2d(3, CHANNELS, kernel_size=K, stride=STRIDE),
#    nn.Conv2d(CHANNELS, 2*CHANNELS, kernel_size=K, stride=STRIDE),
#    nn.Conv2d( 2*CHANNELS, 4*CHANNELS, kernel_size=K, stride=STRIDE),
#    nn.Flatten(),
#    nn.Linear(D, M1), 
#    nn.ReLU(),
#    nn.Linear(M1, C),
#)

model = CNN(IN_CHANNELS, CHANNELS, K, STRIDE, D, M1, C)
print(summary(model, (IN_CHANNELS, H, W)))
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=LR, betas=(0.9, 0.999))  # SGD, Adagrad, RMSprop, Adam

# Mode to GPU
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('Device:', device)
model.to(device)


# Data Batches
batch_size = 128
# The input has to be a dataset from the clas - torch.utils.data.Dataset
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size,
                                           shuffle=True)
test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                           batch_size=batch_size,
                                           shuffle=False)


# Dimensions Exploration, first run one step of the for loop to have data in inputs
for inputs, y in train_loader:
    break

print(inputs.size())
m = nn.Conv2d(1, 32, K, stride=2)
x = m(inputs)
x = F.relu(x)
x = F.dropout(x, p=0.2)
print(x.size())
#x = torch.flatten(x, 1, -1)
#x = torch.max(torch.max(x, 2)[0], 2)[0]
#x = torch.flatten(x, 1, -1) 


# Train the model
n_epochs = 2

def batch_gd(model, criterion, optimizer, train_loader, n_epochs):
    model.train()
    print('Starting Training the model')
    losses = []
    for ii in range(n_epochs):
        t0 = datetime.now()
        for inputs, targets in train_loader:
            # move data to device
            inputs, targets = inputs.to(device), targets.to(device)
            
            # Reshape the inputs from 28x28 image to a D array
            # inputs = inputs.view(-1,D), not needed as it is done in the model
            
            # zero the parameter gradients
            optimizer.zero_grad()
            
            # Forward pass
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            losses.append(loss.item())
            
            # Backward and optimize
            loss.backward()
            optimizer.step()
        
        dt = datetime.now() - t0
        print(f'  iter {ii}: {dt.seconds} seconds')
            
        # for inputs, targets in test_loader:  # to calculate the loss of the test group
    return losses

losses = batch_gd(model, criterion, optimizer, train_loader, n_epochs)

# =============================================================================
# Portada
# =============================================================================

# Loss per iteration
#print(loss.item())
#print(model.bias.data.numpy(), model.weight.data.numpy())
plt.plot(losses, label='Train Loss')
#plt.plot(losses_test, label='Test Loss')
plt.legend()
plt.show()

# Accuracy
model.eval()
n_correct = 0
n_total = 0
y_predict_test = []
for inputs, targets in test_loader:
    inputs, targets = inputs.to(device), targets.to(device)
     
    y_predict = torch.max(model(inputs),1)[1]
    
    n_correct += torch.sum(y_predict == targets).item()
    n_total += len(targets)
    y_predict_test.extend(y_predict.detach().tolist())

conf_m = metrics.confusion_matrix(test_dataset.targets.detach().tolist(), y_predict_test)
print('Confusion Matrix:\n', conf_m)
print(f'Acuracy: {n_correct/n_total:.3f}')






