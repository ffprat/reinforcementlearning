
import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn import metrics

import torch
import torch.nn as nn




n_epochs = 400
LR = 0.1


dataset = datasets.load_iris()
X, y = dataset.data, dataset.target

# Train - Test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

# Standarize
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)


C = len(dataset.target_names)
D = X.shape[1]


# One-hot encoding
def one_hot(y, c):
    """ Transform a 1dim array into a (N, C) array following the one-hot encoding"""
    if y.ndim >= 2:
        raise Exception('y has to be a 1dim array')
    y_hot = np.zeros((len(y), c))
    y_hot[np.arange(len(y)), y] = 1
    return y_hot

y_train = one_hot(y_train, C)
y_test = one_hot(y_test, C)


# Data to Torch 
X_train = torch.from_numpy(X_train.astype(np.float32))
X_test = torch.from_numpy(X_test.astype(np.float32))
y_train = torch.from_numpy(y_train.astype(np.float32))
y_test = torch.from_numpy(y_test.astype(np.float32))


# Model
model = nn.Sequential(
    nn.Linear(D, C),  # Input dim, output dim
    # nn.Softmax(1)
)
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=LR, betas=(0.9, 0.999))  # SGD, Adagrad, RMSprop, Adam


# Train the model
losses = []
losses_test = []
for _ in range(n_epochs):
    optimizer.zero_grad()
    
    # Forward pass
    outputs = model(X_train)
    loss = criterion(outputs, y_train)
    losses.append(loss.item())
    
    # Get test loss
#    outputs = model(X_test)
#    loss = criterion(outputs, y_test)
#    losses_test.append(loss.item())
    
    # Backward and optimize
    loss.backward()
    optimizer.step()
    

# Portada
#print(loss.item())
#print(model.bias.data.numpy(), model.weight.data.numpy())
plt.plot(losses, label='Train Loss')
#plt.plot(losses_test, label='Test Loss')
plt.legend()
plt.show()

y_predict = torch.max(model(X_test),1)[1]
y_test = torch.max(y_test,1)[1]
# .detach().numpy()
# y_predict = np.argmax(y_predict, axis=1)

conf_m = metrics.confusion_matrix(y_test, y_predict)
accuracy = torch.sum(y_test==y_predict).item()/len(y_test)

print('Confusion Matrix:\n', conf_m)
print(f'Accuracy: {accuracy:.3f}')

model_dict = model.state_dict()
# bias = model_dict['0.bias'].detach().numpy()
# weigths = model_dict['0.weight'].detach().numpy()
    

# Save the model
if False:
    torch.save(model_dict, 'logistic_model.pt')

# Load the model
if False:
    model2 = nn.Sequential(
        nn.Linear(D, C),  # Input dim, output dim
        nn.Softmax(1)
    )
    model2.load_state_dict(torch.load('logistic_model.pt'))
    with torch.no_grad():
        y_predict = model2(X_test)
        




