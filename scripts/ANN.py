# -*- coding: utf-8 -*-
"""
Feedforward Artifical Neural Networks
    y_hat = sigmoid((sigmoid((sigmoid(Xw))w))w)
    model = sequence (linear, sigmoid, linear, sigmoid ...)
    
Activation Functions, important the derivate of the function
    Sigmoid: 1/(1+exp(-x)), centered around 0.5 (not ideal), and the vanishing gradient problem
    Hyperbolic tangent: tanh(x)=(exp(2x)-1)/(exp(2x)+1), centered around 0, vanishing gradient problem too
    ReLU: max(0,x), derivate is 0 or 1. Problem of dead neurons if x<0  and not centered
    ReLU variants: LeakyReLU, ELU, softplus
    
Output Function = Final Activation Function
    Regression: None
    Binary Classification: Sigmoid
    Multiclass Classification: Softmax
    
Example
    nn.Sequential(
        nn.Linear(D,M),
        nn.ReLU(1),
        nn.Linear(M, K),
        nn.Softmax(1)
    )
    Although, the softmax function is not really needed as the cost fuction already incorporates it, just watch the output
    
Batch Gradietn Descent: split the whole dataset into differnt batches
    If cound be created with a regular functuion but using yield instead of return (it creates an interable object)
    ```
    train_loades = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=BATCH_SIZE,
                                               shuffle=True)
    for _ in range(n_epochs):
        for X_batch, y_batch in train_loades:
            train
    ```
    

Links
    https://playground.tensorflow.org/
"""

import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn import metrics

import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
# import torchvision.transforms.functional as TF
# TF.to_tensor(train_dataset.data)


# Hyperparameters
LR = 0.01


# Load data, data is sotred in data and targets
train_dataset = torchvision.datasets.MNIST(
    root='../data',
    train=True,
    download=True
)
test_dataset = torchvision.datasets.MNIST(
    root='../data',
    train=False,
    download=True
)
print(train_dataset.data.max())
print(train_dataset.data.shape)
print(test_dataset.data.shape)
ii=2
im = train_dataset.data[ii].numpy()
plt.imshow(im, cmap='gray')

D = train_dataset.data.shape[1] * train_dataset.data.shape[2]
C = len(train_dataset.classes)


# TransformData model
"""This is used in the DataLoader"""
train_dataset.transform = transforms.ToTensor()
test_dataset.transform = transforms.ToTensor()


# Define model
M1 = 128
#model = nn.Sequential(
#    nn.Linear(D, M1), 
#    nn.ReLU(),
#    nn.Linear(M1, C),
#    # nn.Softmax(), not needed as the loss function already incorporates it, the result won't be a vector of probabilities
#)

# Create a Custom Module 
class ANN(nn.Module):
    """ Creates a custom model for de model: linear -> ReLU -> linear """
    def __init__(self, D, M1, C):
        super().__init__()  # super(ANN, self)
        self.dense = nn.Sequential(
            nn.Linear(D, M1),
            nn.ReLU(),
            nn.Linear(M1, C),
        )
    def forward(self, x):
        x = torch.flatten(x, 1, -1)  # keep the first dimension (number of observations)
        x = self.dense(x)
        return x

model = ANN(D, M1, C)
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=LR, betas=(0.9, 0.999))  # SGD, Adagrad, RMSprop, Adam

# Mode to GPU
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('Device:', device)
model.to(device)


# Data Batches
batch_size = 128
# The input has to be a dataset from the clas - torch.utils.data.Dataset
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size,
                                           shuffle=True)
test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                           batch_size=batch_size,
                                           shuffle=False)


# Train the model
n_epochs = 2
losses = []
for ii in range(n_epochs):
    print(f'  iter: {ii}')
    for inputs, targets in train_loader:
        # move data to device
        inputs, targets = inputs.to(device), targets.to(device)
        
        # Reshape the inputs from 28x28 image to a D array
        # inputs = inputs.view(-1,D), not needed as it is done in the model
        
        # zero the parameter gradients
        optimizer.zero_grad()
        
        # Forward pass
        outputs = model(inputs)
        loss = criterion(outputs, targets)
        losses.append(loss.item())
        
        # Backward and optimize
        loss.backward()
        optimizer.step()
        
    # for inputs, targets in test_loader:  # to calculate the loss of the test group


# Portada
#print(loss.item())
#print(model.bias.data.numpy(), model.weight.data.numpy())
print('Train Loss')
plt.plot(losses, label='Train Loss')
#plt.plot(losses_test, label='Test Loss')
plt.legend()
plt.show()

n_correct = 0
n_total = 0
y_predict_test = []
for inputs, targets in test_loader:
    inputs, targets = inputs.to(device), targets.to(device)
     
    y_predict = torch.max(model(inputs),1)[1]
    
    n_correct += torch.sum(y_predict == targets).item()
    n_total += len(targets)
    y_predict_test.extend(y_predict.detach().tolist())

conf_m = metrics.confusion_matrix(test_dataset.targets.detach().tolist(), y_predict_test)
print('Confusion Matrix:\n', conf_m)
print(f'Acuracy: {n_correct/n_total:.3f}')







