"""
Dynamic Programming

The code solves the Bellman Equation
  Q_policy(s,a) = sum_s2( p(s2 | s, a) * (r + GAMMA * V(s2)))
  V_policy(s) = sum_a( p(a|s) * Q_policy(s,a) )

The second control funcion uses an iterative aproach to solve the Bellman eq.

"""
import random
import numpy as np
from copy import deepcopy

import matplotlib.pyplot as plt

ROWS = 3
COLS = 4
ACTIONS = ['U','D','R','L']
GAMMA = 1  # Discount factor (=0.9)
EPS = 0.2  # Epsilon-Greedy method
SMALL_ENOUGH = 1e-3 # threshold for convergence
PENALTY = -0.5


# =============================================================================
# Funcs
# =============================================================================
map_actions = {}
for ii, action in enumerate(ACTIONS):
    map_actions[action] = ii


def new_status(g, s, a, b_windy=True):
    if a not in g.valid_actions(s):
        print('Not a valid action')
        raise Exception('Not a valid action')
    x = s[0]
    y = s[1]
    if a=='U':
        x += -1
    elif a=='D':
        x += 1
    elif a=='R':
        y += 1
    elif a=='L':
        y += -1
    prob_s2 = [[0] * COLS for _  in range(ROWS)]
    prob_s2[x][y] = 1
    
    # Windy
    if s[0]==1 and s[1]==2 and a=='U' and b_windy:
        prob_s2 = [[0] * COLS for _  in range(ROWS)]
        prob_s2[x][y] = 0.5
        prob_s2[1][3] = 0.5
    
    # New status
    p_new_s = [prob_s2[i][j] for i, j in g.all_states if prob_s2[i][j]]
    p_new_s /= np.sum(p_new_s)
    new_s = [(i, j) for i, j in g.all_states if prob_s2[i][j]]
    ii_a = np.argmax(np.random.rand() <= np.cumsum(p_new_s))
    s = new_s[ii_a]
    return prob_s2, s


def to_string(V, type_element='float') -> str:
    rows_str: list[str] = []
    for row in V:
        if type_element=='float':
            row = ["{: .2f}".format(col) for col in row]  # str(round(col,2))
        elif type_element=='int':
            row = ["{:>7}".format(col) for col in row]
        elif type_element=='object':
            pass
        rows_str.append("  ".join(row))
    return "\n".join(rows_str)




# =============================================================================
# Classes
# =============================================================================
class Gridworld():
    
    def __init__(self, rows: int, cols: int):
        """Defines 3-dim array:
            Dim2: rows
            Dim3: cols
        """

        # Terminal States
        terminal = [[0] * cols for _  in range(rows)]
        terminal[0][cols-1] = 1
        terminal[1][cols-1] = 1
        self.terminal = terminal
        
        # Impossible States
        impossible = [[0] * cols for _  in range(rows)]
        impossible[1][1] = 1
        self.impossible = impossible
        
        # Rewards
        rewards = [[PENALTY] * cols for _  in range(rows)]
        rewards[0][cols-1] = 1
        rewards[1][cols-1] = -1
        self.rewards = rewards
        
        # Valid Actions
        self.actions_space = [
            [['R','D'],['R','L'],['R','D','L'],[]],
            [['U','D'],[],['U','D','R'],[]],
            [['R','U'],['R','L'],['R','U','L'],['U','L']],
        ]
        
        # All states
        all_states = []
        all_states_not_terminal = []
        for i in range(rows):
            for j in range(cols):
                if self.is_impossible((i,j)): continue
                all_states.append((i,j))
                if self.is_terminal((i,j)): continue
                all_states_not_terminal.append((i,j))
        self.all_states =  all_states
        self.all_states_not_terminal = all_states_not_terminal
        
        
    def is_terminal(self, s):
        return self.terminal[s[0]][s[1]]
    
    def is_impossible(self, s):
        return self.impossible[s[0]][s[1]]
    
    def valid_actions(self, s):
        return self.actions_space[s[0]][s[1]]
    
    def reward(self, s):
        return self.rewards[s[0]][s[1]]
    
    def transition_probs(self, s2, a, s):
        """Returns p(s2, r | a, s). For deterministic rewards == p(s2 | a, s)
            Useful for the Bellman equation
        """
        if not a in self.valid_actions(s): return 0
        prob_s2, _ = new_status(self, s, a, b_windy=True)
        return prob_s2[s2[0]][s2[1]]
    
    def pi_a(self, a, s, policy):
        """Returns pi(a | s) for a given policy
            Useful for the Bellman equation
        """
        return policy[s[0]][s[1]] == a

    
    
    


# =============================================================================
# Policy Evaluation - Prediction Problem
# =============================================================================

def policy_evaluation(g, V, policy, max_iters=100):
    """Solves Bellman equation using Iterative Policy Evaluation"""
    V = deepcopy(V)
    for k in range(max_iters):  # Iterations
        biggest_change = 0
        for i, j in g.all_states:  # All states 
            if g.is_terminal((i,j)): continue
            s = (i, j)
            v_old = V[i][j]
            
            ## Sum Bellman equation
            new_v = 0
            for a in g.valid_actions(s):
                if not a in g.valid_actions(s): continue
                for i2, j2 in g.all_states:
                    s2 = (i2, j2)
                    new_v += g.pi_a(a, s, policy) * g.transition_probs(s2, a, s) * (g.reward(s2)+GAMMA*V[i2][j2])
            V[i][j] = new_v
            biggest_change = max(biggest_change, np.abs(v_old - new_v))
        
        if biggest_change < SMALL_ENOUGH:
            break
    return V


# =============================================================================
# Best Policy - Control Problem
# =============================================================================


def policy_improvement(g, V, policy, max_iters=100, update_V=True):
    V = deepcopy(V)
    policy = deepcopy(policy)
    for k in range(max_iters):
        if update_V:
            V = policy_evaluation(g, V, policy)
        is_policy_stable = True
        for i, j in g.all_states_not_terminal:
            s = (i,j)
            # a_old = ACTIONS[np.argmax(policy[i][j])]
            a_old = policy[i][j]
            Q = []
            valid_actions = g.valid_actions(s)
            for a in valid_actions:
                sum_q = 0
                for i2, j2 in g.all_states:
                    s2 = (i2, j2)
                    sum_q += g.transition_probs(s2, a, s) * (g.reward(s2)+GAMMA*V[i2][j2])
                Q.append(sum_q)
            a_star = valid_actions[np.argmax(Q)]
            # p_action = [int(a_star==x) for x in ACTIONS]
            policy[s[0]][s[1]] = a_star
            if a_old != a_star: 
                is_policy_stable = False
        if is_policy_stable: break
    return policy, V

def policy_value_iteration(g, V, policy):
    V = deepcopy(V)
    policy = deepcopy(policy)
    for k in range(100):
        biggest_change = 0
        for i, j in g.all_states_not_terminal:
            s = (i,j)
            v_old = V[i][j]
            v_new = float('-Inf')
            
            # Calcultar v_new
            for a in g.valid_actions(s):
                v_s = 0
                for i2, j2 in g.all_states:
                    s2 = (i2, j2)
                    v_s += g.transition_probs(s2, a, s) * (g.reward(s2)+GAMMA*V[i2][j2])
                # Keep the best v
                if v_s > v_new:
                    v_new = v_s
                    
            V[i][j] = v_new
            biggest_change = max(biggest_change, np.abs(v_old - v_new))
        if biggest_change < SMALL_ENOUGH:
            break
    policy, _ = policy_improvement(g, V, policy, max_iters=1, update_V=False)
    return policy, V

        
    

# =============================================================================
# MAIN
# =============================================================================
if __name__ == '__main__':
    
    g = Gridworld(ROWS, COLS)
    
    # Initial policy with random actions
    policy = [['X'] * COLS for _  in range(ROWS)]
    for i, j in g.all_states_not_terminal:
        policy[i][j] = random.choice(g.valid_actions((i,j)))
        
    # policy = [
    #     ['R','R','R','X'],
    #     ['U','X','U','X'],
    #     ['R','R','U','U'],
    # ]

    # Init V and Q with 0
    V = [[0] * COLS for _  in range(ROWS)]
    Q = [[[0] * len(ACTIONS) for _  in range(COLS)] for _ in range(ROWS)]
    
    # TODO
    # Policy Evaluation 
    # V = policy_evaluation(g, V, policy, max_iters=100)
    
    # Find Best policy - Control
    policy, V = policy_improvement(g, V, policy)
    # policy, V = policy_value_iteration(g, V, policy)
    
    
    # Portada 
    print(f'\nGamma: {GAMMA}')   
    print('\nRewards:')
    print(to_string(g.rewards, type_element='float'))
    
    print('\nValue, expected sum of future rewards')
    print(to_string(V))
    # print('\nValue, from policy evaluation')
    # print(to_string(V_star))
    
    print('\nFinal policy:')
    print(to_string(policy, type_element='object'))
    

    # plt.plot(reward_per_episode)
    # plt.title('Reward per episode')
    # plt.show()

        
